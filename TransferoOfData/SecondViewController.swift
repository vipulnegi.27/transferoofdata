//
//  SecondViewController.swift
//  TransferoOfData
//
//  Created by cl-macmini-120 on 18/01/19.
//  Copyright © 2019 cl-macmini-120. All rights reserved.
//

import UIKit

protocol PassData {
    func passData(str: String)
}

class SecondViewController: UIViewController {
    
    var data = ""
    var delegateData: PassData! = nil

    @IBOutlet weak var textName1: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textName1.text = data

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonTwo(_ sender: UIButton) {
        if textName1.text != ""{
            delegateData.passData(str: textName1.text!)
            self.dismiss(animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
