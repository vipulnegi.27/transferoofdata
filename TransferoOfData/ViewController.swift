//
//  ViewController.swift
//  TransferoOfData
//
//  Created by cl-macmini-120 on 18/01/19.
//  Copyright © 2019 cl-macmini-120. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PassData{

    func passData(str: String) {
        self.textName.text = str
    }
    
    
    @IBOutlet weak var textName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func buttonOne(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController{
            vc.data = textName.text ?? "no input"
            self.present(vc, animated: true, completion: nil)
            vc.delegateData = self
        }
    }
    
}

